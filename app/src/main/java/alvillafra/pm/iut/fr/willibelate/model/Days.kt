package alvillafra.pm.iut.fr.willibelate.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


const val NEW_DAY_ID = 0L


@Entity
data class Days(var monday : Boolean = false,
                var tuesday : Boolean = false,
                var wednesday : Boolean = false,
                var thursday : Boolean = false,
                var friday : Boolean = false,
                var saturday : Boolean = false,
                var sunday : Boolean = false,
                @PrimaryKey(autoGenerate=true) val id : Long = NEW_DAY_ID) : Serializable
