package alvillafra.pm.iut.fr.willibelate.http.api

import alvillafra.pm.iut.fr.willibelate.http.dto.JourneyListDto
import alvillafra.pm.iut.fr.willibelate.http.dto.PlaceListDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface SncfApiService {



    @Headers("Authorization:70ce2fb7-77b4-4392-8779-1433cad0f10b", "Content-Type:application/json" )
    @GET("coverage/sncf/journeys?datetime_represents=departure&departure_time")
    fun jouneyList(@Query("from") departureId : String,
                   @Query("to") arrivalId : String,
                   @Query("departure_time") departureTime : String
    ) : Call<JourneyListDto>


    @Headers("Authorization:70ce2fb7-77b4-4392-8779-1433cad0f10b", "Content-Type:application/json" )
    @GET("coverage/sncf/places?type[]=stop_area")
    fun placeList(@Query("q") query : String) : Call<PlaceListDto>

}