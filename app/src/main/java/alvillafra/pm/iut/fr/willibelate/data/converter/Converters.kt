package alvillafra.pm.iut.fr.willibelate.data.converter

import alvillafra.pm.iut.fr.willibelate.model.Days
import alvillafra.pm.iut.fr.willibelate.model.Station
import alvillafra.pm.iut.fr.willibelate.model.Time
import androidx.room.TypeConverter
import java.lang.RuntimeException
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*

class Converters {


    @TypeConverter
    fun timeFromString(str : String) : Time?{
        if(str.isNullOrEmpty()){
            return null
        }
        val values = str.split(Regex(":"))
        if(values.size != 2){
            RuntimeException()
        }
        return Time(values[0].toInt(),values[1].toInt())
    }
    @TypeConverter
    fun timeToString(time : Time) : String{
        val builder = StringBuilder()

        builder.append(time.hour)
        builder.append(':')
        builder.append(time.minute)
        return builder.toString()
    }

    @TypeConverter
    fun daysFromString(str : String) : Days?{
        if(str.isNullOrEmpty()){
            return null
        }

        val values = str.split(Regex(";"))
        if(values.size != 7){
            RuntimeException()
        }

        return Days(values[0] == "1",
            values[1] == "1",
            values[2] == "1",
            values[3] == "1",
            values[4] == "1",
            values[5] == "1",
            values[6] == "1")
    }

    @TypeConverter
    fun daysToString(days : Days) : String {
        val builder = StringBuilder()

        if (days.monday) builder.append("1;") else builder.append("0;")
        if (days.tuesday) builder.append("1;") else builder.append("0;")
        if (days.wednesday) builder.append("1;") else builder.append("0;")
        if (days.thursday) builder.append("1;") else builder.append("0;")
        if (days.friday) builder.append("1;") else builder.append("0;")
        if (days.saturday) builder.append("1;") else builder.append("0;")
        if (days.sunday) builder.append("1") else builder.append("0")

        return builder.toString()
    }

    @TypeConverter
    fun stationFromString(str : String) : Station?{
        if(str.isNullOrEmpty()){
            return null
        }
        val values = str.split(Regex(";"))
        if(values.size != 2){
            RuntimeException()
        }

        return Station(values[0],values[1])
    }

    @TypeConverter
    fun stationToString(station : Station) : String{

        val builder = StringBuilder()

        builder.append(station.stationId)
        builder.append(';')
        builder.append(station.name)

        return builder.toString()
    }

}
