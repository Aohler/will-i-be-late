package alvillafra.pm.iut.fr.willibelate.view.adapter

import alvillafra.pm.iut.fr.willibelate.R
import alvillafra.pm.iut.fr.willibelate.databinding.ItemJourneyResultBinding
import alvillafra.pm.iut.fr.willibelate.model.Journey
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_journey_result.view.*

class JourneyResultItemAdapter(private val listener : Callback )
    : ListAdapter<Journey,JourneyResultItemAdapter.JourneyViewHolder>(JourneyDiffCallBack()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JourneyViewHolder
            = JourneyViewHolder(ItemJourneyResultBinding.inflate(LayoutInflater.from(parent.context)),listener)


    override fun onBindViewHolder(holder: JourneyViewHolder, position: Int)
            = holder.bind(getItem(position))


    interface Callback{
        fun onAddButtonClicked(journey : Journey)
    }


    class JourneyViewHolder(val binding : ItemJourneyResultBinding, var listener: Callback)
        : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.findViewById<Button>(R.id.add_button).setOnClickListener{ binding.journey?.let{ listener.onAddButtonClicked(it)}}
        }

        fun bind(item : Journey){
            binding.journey = item
            binding.executePendingBindings()
        }
    }

    class JourneyDiffCallBack() : DiffUtil.ItemCallback<Journey>(){
        override fun areItemsTheSame(oldItem: Journey, newItem: Journey): Boolean = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Journey, newItem: Journey): Boolean = oldItem == newItem
    }
}