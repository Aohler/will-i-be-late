package alvillafra.pm.iut.fr.willibelate.view.fragment

import alvillafra.pm.iut.fr.willibelate.R
import alvillafra.pm.iut.fr.willibelate.data.converter.Converters
import alvillafra.pm.iut.fr.willibelate.http.ApiUtils
import alvillafra.pm.iut.fr.willibelate.http.api.SncfApiService
import alvillafra.pm.iut.fr.willibelate.http.converter.DtoConverters
import alvillafra.pm.iut.fr.willibelate.http.dto.JourneyListDto
import alvillafra.pm.iut.fr.willibelate.http.dto.PlaceDto
import alvillafra.pm.iut.fr.willibelate.http.dto.PlaceListDto
import alvillafra.pm.iut.fr.willibelate.model.Journey
import alvillafra.pm.iut.fr.willibelate.model.Time
import alvillafra.pm.iut.fr.willibelate.view.adapter.JourneySearchListAdapter
import alvillafra.pm.iut.fr.willibelate.viewmodel.JourneySearchViewModel
import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.google.android.material.snackbar.Snackbar

import kotlinx.android.synthetic.main.fragment_journey_search.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

import javax.security.auth.callback.Callback



class JourneySearchFragment : Fragment() {

    private lateinit var navController : NavController

    private var sncfApiService: SncfApiService? = null

    private lateinit var searchJourneyVM : JourneySearchViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchJourneyVM = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                val departure = MutableLiveData<List<PlaceDto>>()
                departure.value = ArrayList()
                val arrival = MutableLiveData<List<PlaceDto>>()
                arrival.value = ArrayList()
                return JourneySearchViewModel(departure,arrival, MutableLiveData(), MutableLiveData(),
                    MutableLiveData()
                ) as T
            }
        }).get(JourneySearchViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = alvillafra.pm.iut.fr.willibelate.databinding.FragmentJourneySearchBinding.inflate(inflater)
        binding.journeySearchVM = searchJourneyVM
        binding.lifecycleOwner = this
        sncfApiService = ApiUtils.getSncfApiService()
        navController = Navigation.findNavController(activity!!, alvillafra.pm.iut.fr.willibelate.R.id.nav_host_fragment)


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        configureAutoCompleteTextView(searchJourneyVM.departureStopAreas, searchJourneyVM.departurePlace, departure_stop_area_edit_text)
        configureAutoCompleteTextView(searchJourneyVM.arrivalStopAreas, searchJourneyVM.arrivalPlace, arrival_stop_area_edit_text)
        configureDateEditText()
        configureSearchButton()
    }


    private fun configureDateEditText(){

        departure_date_edit_text.keyListener = null

        val currentTime = Calendar.getInstance()
        val hour = currentTime.get(Calendar.HOUR_OF_DAY)
        val minute = currentTime.get(Calendar.MINUTE)
        departure_date_edit_text.setOnClickListener{ view  ->
            val timePicker = TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { _, p1, p2 ->
                searchJourneyVM.departureTime.value = Time(p1,p2)
            },hour,minute, true)
            timePicker.setTitle(alvillafra.pm.iut.fr.willibelate.R.string.departure_time_text)
            timePicker.show()
        }
    }

    private fun configureAutoCompleteTextView(placeList : MutableLiveData<List<PlaceDto>>,place : MutableLiveData<PlaceDto>, autoCompleteTextView: AutoCompleteTextView){
        autoCompleteTextView.doOnTextChanged { text, _, _ ,_ -> updateStopAreas(text.toString(),placeList) }
        autoCompleteTextView.setAdapter(JourneySearchListAdapter(context!!,
            alvillafra.pm.iut.fr.willibelate.R.layout.item_place_autocomplete,placeList.value))
        placeList.observe(this,
            Observer<List<PlaceDto>> { list ->
                val adapter = JourneySearchListAdapter(context!!, alvillafra.pm.iut.fr.willibelate.R.layout.item_place_autocomplete, list)
                autoCompleteTextView.setAdapter(adapter)
                adapter.notifyDataSetChanged()
            })
        autoCompleteTextView.setOnItemClickListener { _, _, p2, _ ->
            val item = (autoCompleteTextView.adapter as JourneySearchListAdapter).getItem(p2)
            place.value = item
        }
    }

    private fun configureSearchButton() {
        search_button.setOnClickListener{ view ->
            if(searchJourneyVM.departurePlace.value != null && searchJourneyVM.arrivalPlace.value != null && searchJourneyVM.departureTime.value != null){
                search_button.visibility = View.GONE
                search_progress.visibility = View.VISIBLE
                val departureDate = getStringDepartureTime(searchJourneyVM.departureTime.value)


                sncfApiService?.jouneyList(searchJourneyVM.departurePlace.value?.stop_area!!.administrative_regions[0].id,
                    searchJourneyVM.arrivalPlace.value?.stop_area!!.administrative_regions[0].id,
                    departureDate)?.enqueue(object : Callback, retrofit2.Callback<JourneyListDto> {
                    override fun onFailure(call: Call<JourneyListDto>, t: Throwable) {
                        search_button.visibility = View.VISIBLE
                        search_progress.visibility = View.GONE
                        Snackbar.make(view!!,R.string.search_error,Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                    }
                    override fun onResponse(call: Call<JourneyListDto>, response: Response<JourneyListDto>) {
                        if(response.isSuccessful){
                            val bundle = Bundle()
                            bundle.putSerializable(JourneyResultsFragment.JOURNEY_LIST,
                                DtoConverters.listJourneyFromListJourneyDto(response.body()!!.journeys) as ArrayList<Journey>)
                            navController.navigate(R.id.journeyResultsFragment,bundle)
                        }
                    }
                })
            }
            else{
                search_button.visibility = View.VISIBLE
                search_progress.visibility = View.GONE
                Snackbar.make(view!!,R.string.search_not_all_values_text,Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            }
        }
    }

    private fun getStringDepartureTime(time : Time?) : String {
        if(time == null){
            return ""
        }
        val current = Calendar.getInstance()
        if(current.get(Calendar.HOUR_OF_DAY) > time.hour){
            current.add(Calendar.DAY_OF_MONTH,1)
        }
        current.set(Calendar.HOUR_OF_DAY, time.hour)
        current.set(Calendar.MINUTE, time.minute)

        return DtoConverters.dateToString(current.time)
    }

    private fun updateStopAreas(text : String, placeList : MutableLiveData<List<PlaceDto>>) {
            sncfApiService?.placeList(text)?.enqueue(object : Callback, retrofit2.Callback<PlaceListDto> {
                override fun onFailure(call: Call<PlaceListDto>, t: Throwable) {
                    Snackbar.make(view!!,R.string.places_error,Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }
                override fun onResponse(call: Call<PlaceListDto>, response: Response<PlaceListDto>) {
                    if(response.isSuccessful){
                        if(response.body()?.places != null){
                            placeList.value = response.body()?.places
                        }
                    }
                }
            })
    }



}

