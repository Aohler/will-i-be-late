package alvillafra.pm.iut.fr.willibelate.http

import alvillafra.pm.iut.fr.willibelate.http.api.SncfApiService

class ApiUtils {

    companion object {
        val BASE_URL = "https://api.navitia.io/v1/"

        fun getSncfApiService():SncfApiService? {
            return RetrofitClient.getClient(BASE_URL)?.create(SncfApiService::class.java)
        }
    }

}