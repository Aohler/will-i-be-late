package alvillafra.pm.iut.fr.willibelate.viewmodel


import alvillafra.pm.iut.fr.willibelate.http.dto.PlaceDto
import alvillafra.pm.iut.fr.willibelate.model.Time
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*

class JourneySearchViewModel(val departureStopAreas : MutableLiveData<List<PlaceDto>>,
                             val arrivalStopAreas : MutableLiveData<List<PlaceDto>>,
                             val departurePlace : MutableLiveData<PlaceDto>,
                             val arrivalPlace : MutableLiveData<PlaceDto>,
                             val departureTime : MutableLiveData<Time>) : ViewModel()