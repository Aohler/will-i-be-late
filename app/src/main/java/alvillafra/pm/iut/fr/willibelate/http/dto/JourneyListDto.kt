package alvillafra.pm.iut.fr.willibelate.http.dto

import android.content.Context
import java.io.Serializable

data class JourneyListDto(
    val context: ContextDto,
    val disruptions: List<Any>,
    val exceptions: List<Any>,
    val feed_publishers: List<Any>,
    val journeys: List<JourneyDto>,
    val links: List<LinkDto>,
    val notes: List<Any>,
    val tickets: List<Any>
) : Serializable



data class CarDirectPathDto(
    val co2_emission: Co2EmissionDto
) : Serializable

data class Co2EmissionDto(
    val unit: String,
    val value: Double
) : Serializable

data class JourneyDto(
    val arrival_date_time: String,
    val calendars: List<CalendarDto>,
    val co2_emission: Co2EmissionX,
    val departure_date_time: String,
    val distances: DistancesDto,
    val duration: Int,
    val durations: DurationsDto,
    val fare: FareDto,
    val links: List<LinkDto>,
    val nb_transfers: Int,
    val requested_date_time: String,
    val sections: List<SectionDto>,
    val status: String,
    val tags: List<String>,
    val type: String
) : Serializable

data class CalendarDto(
    val active_periods: List<ActivePeriodDto>,
    val week_pattern: WeekPatternDto
) : Serializable

data class WeekPatternDto(
    val friday: Boolean,
    val monday: Boolean,
    val saturday: Boolean,
    val sunday: Boolean,
    val thursday: Boolean,
    val tuesday: Boolean,
    val wednesday: Boolean
) : Serializable

data class ActivePeriodDto(
    val begin: String,
    val end: String
)

data class DistancesDto(
    val bike: Int,
    val car: Int,
    val ridesharing: Int,
    val walking: Int
) : Serializable


data class SectionDto(
    val additional_informations: List<String>,
    val arrival_date_time: String,
    val base_arrival_date_time: String,
    val base_departure_date_time: String,
    val co2_emission: Co2EmissionX,
    val data_freshness: String,
    val departure_date_time: String,
    val display_informations: DisplayInformationsDto,
    val duration: Int,
    val from: From,
    val geojson: GeojsonDto,
    val id: String,
    val links: List<Any>,
    val mode: String,
    val stop_date_times: List<StopDateTimeDto>,
    val to: ToDto,
    val type: String
) : Serializable

data class DisplayInformationsDto(
    val code: String,
    val color: String,
    val commercial_mode: String,
    val description: String,
    val direction: String,
    val equipments: List<Any>,
    val headsign: String,
    val label: String,
    val links: List<Any>,
    val name: String,
    val network: String,
    val physical_mode: String,
    val text_color: String
) : Serializable

data class ToDto(
    val administrative_region: AdministrativeRegionDto,
    val distance: String,
    val embedded_type: String,
    val id: String,
    val name: String,
    val quality: Int
) : Serializable


data class GeojsonDto(
    val coordinates: List<Any>,
    val properties: List<PropertyDto>,
    val type: String
) : Serializable

data class PropertyDto(
    val length: Int
) : Serializable

data class Co2EmissionX(
    val unit: String,
    val value: Double
) : Serializable

data class StopDateTimeDto(
    val additional_informations: List<Any>,
    val arrival_date_time: String,
    val base_arrival_date_time: String,
    val base_departure_date_time: String,
    val departure_date_time: String,
    val links: List<Any>,
    val stop_point: StopPointDto
) : Serializable

data class StopPointDto(
    val coord: CoordDto,
    val equipments: List<Any>,
    val fare_zone: FareZoneDto,
    val id: String,
    val label: String,
    val links: List<Any>,
    val name: String
) : Serializable

data class FareZoneDto(
    val name: String
) : Serializable

data class From(
    val distance: String,
    val embedded_type: String,
    val id: String,
    val name: String,
    val quality: Int,
    val stop_point: StopPointXDto
)  : Serializable

data class StopPointXDto(
    val administrative_regions: List<AdministrativeRegionDto>,
    val coord: CoordDto,
    val equipments: List<Any>,
    val fare_zone: FareZoneDto,
    val id: String,
    val label: String,
    val links: List<Any>,
    val name: String,
    val stop_area: StopAreaDto
) : Serializable


data class FareDto(
    val found: Boolean,
    val links: List<Any>,
    val total: TotalDto
) : Serializable

data class TotalDto(
    val value: String
) : Serializable

data class DurationsDto(
    val bike: Int,
    val car: Int,
    val ridesharing: Int,
    val total: Int,
    val walking: Int
) : Serializable