package alvillafra.pm.iut.fr.willibelate.viewmodel

import alvillafra.pm.iut.fr.willibelate.model.Journey
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class JourneyResultListViewModel(val journeyList : MutableLiveData<List<Journey>>) : ViewModel()