package alvillafra.pm.iut.fr.willibelate.viewmodel

import alvillafra.pm.iut.fr.willibelate.data.JourneyDatabase
import alvillafra.pm.iut.fr.willibelate.data.repository.JourneyRepository
import alvillafra.pm.iut.fr.willibelate.model.Days
import alvillafra.pm.iut.fr.willibelate.model.Journey
import alvillafra.pm.iut.fr.willibelate.model.Time
import android.provider.SyncStateContract.Helpers.update
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel


class JourneyViewModel(val id : Long) : ViewModel() {

    private val repository : JourneyRepository = JourneyRepository(JourneyDatabase.getInstance().journeyDao())

    val journey = repository.loadJourneyById(id)

    val active = MediatorLiveData<Boolean>()
    init{
        active.addSource(journey) { active.postValue(it?.repetition?.active)}
        active.observeForever{ newActive ->
            journey.value?.let {
                if(it.repetition?.active != newActive){
                    it.repetition?.active = newActive
                    update()
                    Log.d("Active","Active changed")
                }
            }
        }
    }

    fun update(){
        journey.value?.let { repository.updateJourney(it) }
    }
}