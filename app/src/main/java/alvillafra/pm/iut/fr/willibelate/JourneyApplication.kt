package alvillafra.pm.iut.fr.willibelate

import alvillafra.pm.iut.fr.willibelate.data.JourneyDatabase
import android.app.Application

class JourneyApplication : Application() {

    override fun onCreate() {
        JourneyDatabase.initialize(this)
        super.onCreate()
    }
}