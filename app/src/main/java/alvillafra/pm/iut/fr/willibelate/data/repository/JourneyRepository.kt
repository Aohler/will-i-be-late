package alvillafra.pm.iut.fr.willibelate.data.repository

import alvillafra.pm.iut.fr.willibelate.data.dao.JourneyDao
import alvillafra.pm.iut.fr.willibelate.model.Journey
import androidx.lifecycle.LiveData

import java.util.concurrent.Executors

class JourneyRepository(val journeyDao : JourneyDao) {

    val IO_EXECUTOR = Executors.newSingleThreadExecutor()

    fun loadAllJourneys() : LiveData<List<Journey>> = journeyDao.loadAllJourneys()

    fun loadJourneyById(id : Long) : LiveData<Journey> = journeyDao.loadJourneyById(id)

    fun insertJourney(journey: Journey){
        IO_EXECUTOR.execute{
            journeyDao.insertJourney(journey)
        }
    }

    fun updateJourney(journey : Journey){
        IO_EXECUTOR.execute{
            journeyDao.updateJourney(journey)
        }
    }

    fun deleteJourney(journey: Journey){
        IO_EXECUTOR.execute{
            journeyDao.deleteJourney(journey)
        }
    }
}