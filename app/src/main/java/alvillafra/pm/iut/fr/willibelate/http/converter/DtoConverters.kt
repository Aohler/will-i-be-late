package alvillafra.pm.iut.fr.willibelate.http.converter

import alvillafra.pm.iut.fr.willibelate.http.dto.CalendarDto
import alvillafra.pm.iut.fr.willibelate.http.dto.JourneyDto
import alvillafra.pm.iut.fr.willibelate.http.dto.SectionDto
import alvillafra.pm.iut.fr.willibelate.model.Days
import alvillafra.pm.iut.fr.willibelate.model.Journey
import alvillafra.pm.iut.fr.willibelate.model.Station
import alvillafra.pm.iut.fr.willibelate.model.Time

import java.text.SimpleDateFormat
import java.util.*

class DtoConverters{

    companion object {

        fun listJourneyFromListJourneyDto(listDto : List<JourneyDto>) : List<Journey> {
            val list = ArrayList<Journey>()
            for (dto in listDto){
                list.add(DtoConverters.journeyFromJourneyDto(dto))
            }
            return list
        }

        fun dateFromString(str : String) : Date?{
            if(str.isNullOrEmpty()){
                return null
            }
            var format = SimpleDateFormat("yyyyMMdd'T'hhmmss")
            return format.parse(str)
        }

        fun dateToString(date : Date?) : String{
            if(date == null){
                return ""
            }
            var format = SimpleDateFormat("yyyyMMdd'T'hhmmss")
            return format.format(date)
        }

        fun journeyFromJourneyDto(dto : JourneyDto) : Journey {
            val maxIndex = dto.sections.size -1
            val journey : Journey
            val departurePlace = DtoConverters.stationFromSectionDto(dto.sections[0])
            val arrivalPlace = DtoConverters.stationFromSectionDto(dto.sections[maxIndex])
            val departureTime = DtoConverters.dateFromString(dto.departure_date_time)
            val arrivalTime = DtoConverters.dateFromString(dto.arrival_date_time)

            val days = DtoConverters.daysFromCalendarDto(dto.calendars)
            journey = Journey( departurePlace, Time(departureTime!!.hours,departureTime!!.minutes), arrivalPlace,Time(arrivalTime!!.hours,arrivalTime!!.minutes),days)
            return journey
        }

        fun daysFromCalendarDto(dto : List<CalendarDto>) : Days{
            var pattern = Days(true,true,true,true,true,true,true)
            for(calendar : CalendarDto in dto){
                if(!calendar.week_pattern.monday) pattern.monday = false
                if(!calendar.week_pattern.tuesday) pattern.tuesday = false
                if(!calendar.week_pattern.wednesday) pattern.wednesday = false
                if(!calendar.week_pattern.thursday) pattern.thursday = false
                if(!calendar.week_pattern.friday) pattern.friday = false
                if(!calendar.week_pattern.saturday) pattern.saturday = false
                if(!calendar.week_pattern.sunday) pattern.sunday = false
            }
            return pattern
        }

        fun stationFromSectionDto(dto : SectionDto) : Station{
            return Station(dto.from.id,dto.from.name)
        }

    }
}
