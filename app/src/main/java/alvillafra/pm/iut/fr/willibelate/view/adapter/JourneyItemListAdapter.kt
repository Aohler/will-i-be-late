package alvillafra.pm.iut.fr.willibelate.view.adapter

import alvillafra.pm.iut.fr.willibelate.databinding.ItemJourneyBinding
import alvillafra.pm.iut.fr.willibelate.model.Journey
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView


class JourneyItemListAdapter(val listener : Callback)
    : ListAdapter<Journey, JourneyItemListAdapter.JourneyViewHolder>(JourneyDiffCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JourneyItemListAdapter.JourneyViewHolder =
        JourneyItemListAdapter.JourneyViewHolder(ItemJourneyBinding.inflate(LayoutInflater.from(parent.context)),listener)

    override fun onBindViewHolder(holder: JourneyItemListAdapter.JourneyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    interface Callback{
        fun onItemCLicked(journey : Journey)
        fun onSwitchActivated(journey : Journey, isActive : Boolean)
    }

    class JourneyViewHolder(val binding : alvillafra.pm.iut.fr.willibelate.databinding.ItemJourneyBinding,
                            val listener : Callback)
        : RecyclerView.ViewHolder(binding.root){
        init {
            binding.root.setOnClickListener{ binding.journey?.let{ listener.onItemCLicked(it) } }
            binding.itemActivatedSwitch.setOnCheckedChangeListener{ _, checked ->
                binding.journey?.let {
                    if(it.repetition.active != checked){
                        listener.onSwitchActivated(it,checked)
                    }
                }
            }
        }

        fun bind(journey : Journey){
            binding.journey = journey
        }

    }

    class JourneyDiffCallBack() : DiffUtil.ItemCallback<Journey>(){
        override fun areItemsTheSame(oldItem: Journey, newItem: Journey): Boolean = oldItem.id == newItem.id
        override fun areContentsTheSame(oldItem: Journey, newItem: Journey): Boolean = oldItem == newItem
    }

}