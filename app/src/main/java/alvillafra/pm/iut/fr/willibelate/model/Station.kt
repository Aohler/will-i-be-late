package alvillafra.pm.iut.fr.willibelate.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

const val NEW_STATION_ID = 0L

@Entity(tableName = "station")
data class Station(@ColumnInfo(name="station_id") val stationId : String,
                   val name : String,
                   @PrimaryKey(autoGenerate=true) val id : Long = NEW_STATION_ID) : Serializable {

}
