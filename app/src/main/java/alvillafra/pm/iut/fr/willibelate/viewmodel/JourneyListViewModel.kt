package alvillafra.pm.iut.fr.willibelate.viewmodel

import alvillafra.pm.iut.fr.willibelate.data.JourneyDatabase
import alvillafra.pm.iut.fr.willibelate.data.repository.JourneyRepository
import alvillafra.pm.iut.fr.willibelate.model.Journey
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class JourneyListViewModel : ViewModel(){

    val journeyList: LiveData<List<Journey>>

    val repository = JourneyRepository(JourneyDatabase.getInstance().journeyDao())

    init {
        journeyList = repository.loadAllJourneys()
    }

    fun update(journey : Journey){
        repository.updateJourney(journey)
    }
}


