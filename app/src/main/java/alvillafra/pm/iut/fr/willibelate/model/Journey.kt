package alvillafra.pm.iut.fr.willibelate.model

import alvillafra.pm.iut.fr.willibelate.data.converter.Converters
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

import java.util.*

const val NEW_JOURNEY_ID = 0L


@Entity(tableName = "journeys")
data class Journey(
    var departureStation : Station,
    @ColumnInfo(name = "departure_time") var departureTime : Time,
    var arrivalStation: Station,
    @ColumnInfo(name = "arrival_time") var arrivalTime : Time,
    var pattern : Days,
    @PrimaryKey(autoGenerate=true) val id : Long = NEW_JOURNEY_ID,
    @Embedded val repetition : Repetition = Repetition(false,Days(false,false,false,false,false, false,false))

) : Serializable {



}