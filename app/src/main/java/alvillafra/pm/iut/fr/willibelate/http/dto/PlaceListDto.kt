package alvillafra.pm.iut.fr.willibelate.http.dto

import java.io.Serializable

data class PlaceListDto(
    val context: ContextDto,
    val disruptions: List<Any>,
    val feed_publishers: List<Any>,
    val links: List<LinkDto>,
    val places: List<PlaceDto>
) : Serializable

data class PlaceDto(
    val distance: String,
    val embedded_type: String,
    val id: String,
    val name: String,
    val quality: Int,
    val stop_area: StopAreaDto
) : Serializable{
    override fun toString(): String {
        return name
    }
}

data class StopAreaDto(
    val codes: List<CodeDto>,
    val coord: CoordDto,
    val id: String,
    val label: String,
    val links: List<LinkDto>,
    val name: String,
    val timezone: String,
    val administrative_regions : List<AdministrativeRegionDto>

) : Serializable

data class AdministrativeRegionDto(
    val coord: CoordDto,
    val id: String,
    val insee: String,
    val label: String,
    val level: Int,
    val name: String,
    val zip_code: String
) : Serializable


data class CodeDto(
    val type: String,
    val value: String
)

data class CoordDto(
    val lat: String,
    val lon: String
) : Serializable

data class ContextDto(
    val car_direct_path: CarDirectPathDto,
    val current_datetime: String,
    val timezone: String
) : Serializable

data class LinkDto(
    val href: String,
    val rel: String,
    val templated: Boolean,
    val type: String
) : Serializable