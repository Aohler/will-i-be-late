package alvillafra.pm.iut.fr.willibelate.data.dao

import alvillafra.pm.iut.fr.willibelate.model.Journey

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface JourneyDao {

    @Query("SELECT * FROM journeys")
    fun loadAllJourneys() : LiveData<List<Journey>>

    @Query("SELECT * FROM journeys WHERE id = :id")
    fun loadJourneyById(id: Long) : LiveData<Journey>

    @Update
    fun updateJourney(journey : Journey)

    @Insert
    fun insertJourney(journey : Journey)

    @Delete
    fun deleteJourney(journey : Journey)

}