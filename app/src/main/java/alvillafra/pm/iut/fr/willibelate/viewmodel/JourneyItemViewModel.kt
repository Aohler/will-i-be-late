package alvillafra.pm.iut.fr.willibelate.viewmodel

import alvillafra.pm.iut.fr.willibelate.data.JourneyDatabase
import alvillafra.pm.iut.fr.willibelate.data.repository.JourneyRepository
import alvillafra.pm.iut.fr.willibelate.model.Journey
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class JourneyItemViewModel(j : Journey) : ViewModel(){

    private val repository = JourneyRepository(JourneyDatabase.getInstance().journeyDao())

    val journey : LiveData<Journey>

    val activated : LiveData<Boolean>

    init{
        journey = MutableLiveData<Journey>()
        journey.value = j

        activated = MediatorLiveData<Boolean>()
        activated.addSource(journey){ it.repetition.active }
        activated.observeForever{ newActivated ->
            journey.value?.let{
                if( it.repetition.active != newActivated){
                    it.repetition.active = newActivated
                    repository.updateJourney(it)
                }
            }
        }

    }
}