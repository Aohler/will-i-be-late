package alvillafra.pm.iut.fr.willibelate.view.fragment

import alvillafra.pm.iut.fr.willibelate.databinding.FragmentRepetitionPatternBinding
import alvillafra.pm.iut.fr.willibelate.model.Journey
import alvillafra.pm.iut.fr.willibelate.viewmodel.JourneyViewModel
import android.content.Context
import android.os.Bundle
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.*

class RepetitionPatternFragment() : DialogFragment(){


    companion object {
        const val JOURNEY = "alvillafra.pm.iut.fr.willibelate.view.fragment.journey"
    }

    private lateinit var journeyVM : JourneyViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val journey = arguments?.getSerializable(JOURNEY) as Journey
        val liveData = MutableLiveData<Journey>()
        liveData.value = journey
        journeyVM = ViewModelProviders.of(this,object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T = JourneyViewModel(1) as T

        }).get(JourneyViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentRepetitionPatternBinding.inflate(inflater)
        binding.journeyVM = journeyVM
        binding.lifecycleOwner = this

        return binding.root
    }
}