package alvillafra.pm.iut.fr.willibelate.view.binding

import alvillafra.pm.iut.fr.willibelate.model.Days
import alvillafra.pm.iut.fr.willibelate.model.Repetition
import alvillafra.pm.iut.fr.willibelate.model.Time
import androidx.databinding.InverseMethod
import androidx.room.util.StringUtil
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*

object BindingConverters {

    @JvmStatic
    @InverseMethod("dateToDisplayableString")
    fun dateFromDisplayableString(str : String) : Date?{
        if(str.isNullOrEmpty()){
            return null
        }
        var format = SimpleDateFormat("dd/MM/yyyy HH:mm")
        return format.parse(str)
    }

    @JvmStatic
    fun dateToDisplayableString(date : Date?) : String{
        if(date == null){
            return ""
        }
        var format = SimpleDateFormat("dd/MM/yyyy HH:mm")
        return format.format(date)
    }

    @JvmStatic
    fun timeToString(date: Time?) : String{
        if(date == null){
            return ""
        }
        val builder = StringBuilder()
        builder.append(date.hour)
        builder.append(':')
        builder.append(date.minute)
        return builder.toString()
    }

    @JvmStatic
    fun repetitionToString(repetition : Repetition?) : String{
        if(repetition == null){
            return ""
        }
        repetition.days?.let{
            if(Locale.getDefault().displayLanguage == "fr")
                return BindingConverters.daysToFrenchString(it)
            else
                return BindingConverters.daysToDefaultString(it)
        }
        return ""
    }

    @JvmStatic
    fun daysToString(days : Days?) : String{
        if(days == null){
            return ""
        }
        if(Locale.getDefault().displayLanguage == "fr")
            return BindingConverters.daysToFrenchString(days)
        else
            return BindingConverters.daysToDefaultString(days)
    }


    private fun daysToFrenchString(days : Days) : String{
        val builder = StringBuilder()
        if(days.monday) builder.append("L")
        if(days.tuesday) builder.append("Ma")
        if(days.wednesday) builder.append("Me")
        if(days.thursday) builder.append("J")
        if(days.friday) builder.append("V")
        if(days.saturday) builder.append("S")
        if(days.sunday) builder.append("D")
        return builder.toString()
    }

    private fun daysToDefaultString(days : Days) : String{
        val builder = StringBuilder()
        if(days.monday) builder.append("M")
        if(days.tuesday) builder.append("Tu")
        if(days.wednesday) builder.append("W")
        if(days.thursday) builder.append("Th")
        if(days.friday) builder.append("F")
        if(days.saturday) builder.append("Sa")
        if(days.sunday) builder.append("Su")
        return builder.toString()
    }

    @JvmStatic
    fun stringToShorterString(str : String?, maxLetters : Int) : String {
        if(str == null){
            return ""
        }

        if(str.length <= maxLetters){
            return str
        }

        val builder = StringBuilder()
        builder.append(str.substring(0,maxLetters))
        builder.append("...")
        return builder.toString()
    }
}