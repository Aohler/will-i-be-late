package alvillafra.pm.iut.fr.willibelate.view.fragment

import alvillafra.pm.iut.fr.willibelate.R
import alvillafra.pm.iut.fr.willibelate.data.JourneyDatabase
import alvillafra.pm.iut.fr.willibelate.data.repository.JourneyRepository
import alvillafra.pm.iut.fr.willibelate.databinding.FragmentJourneyResultsBinding
import alvillafra.pm.iut.fr.willibelate.model.Journey
import alvillafra.pm.iut.fr.willibelate.view.adapter.JourneyResultItemAdapter
import alvillafra.pm.iut.fr.willibelate.viewmodel.JourneyResultListViewModel
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_journey_results.*
import java.lang.RuntimeException
import java.util.ArrayList
import java.util.logging.Logger

class JourneyResultsFragment : Fragment(), JourneyResultItemAdapter.Callback {

    companion object {
        val JOURNEY_LIST = "resultsJourneyList"
    }

    lateinit var listener : JourneyMasterFragment.NavigationListener

    lateinit var navController : NavController

    lateinit var journeyListVM : JourneyResultListViewModel

    val journeyAdapter = JourneyResultItemAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val list = arguments?.getSerializable(JOURNEY_LIST) as ArrayList<Journey>
        val liveData = MutableLiveData<List<Journey>>()
        liveData.value = list
        journeyListVM = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T = JourneyResultListViewModel(liveData) as T

        }).get(JourneyResultListViewModel::class.java)

        journeyListVM.journeyList.observe(this,
            Observer{
                journeyAdapter.submitList(it)
            } )
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if(context is alvillafra.pm.iut.fr.willibelate.view.fragment.JourneyMasterFragment.NavigationListener){
            listener = context
        }
        else{
            RuntimeException("The context doesn't implement NavigationListener !")
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        navController = Navigation.findNavController(activity!!,R.id.nav_host_fragment)
        val binding = FragmentJourneyResultsBinding.inflate(inflater)
        binding.journeyListVM = journeyListVM
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        results_recycler_view.adapter = journeyAdapter

    }

    override fun onAddButtonClicked(journey: Journey) {
        val repo = JourneyRepository(JourneyDatabase.getInstance().journeyDao())
        repo.insertJourney(journey)
        listener.onNavigateToMaster()
    }
}