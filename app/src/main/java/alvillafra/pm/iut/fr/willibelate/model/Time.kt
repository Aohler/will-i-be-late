package alvillafra.pm.iut.fr.willibelate.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

const val NEW_TIME_ID = 0L

@Entity
data class Time(var hour : Int,
                var minute : Int,
                @PrimaryKey(autoGenerate = true) val id : Long = NEW_TIME_ID) : Serializable