package alvillafra.pm.iut.fr.willibelate.view.adapter

import alvillafra.pm.iut.fr.willibelate.R
import alvillafra.pm.iut.fr.willibelate.http.dto.PlaceDto
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.item_place_autocomplete.view.*

class JourneySearchListAdapter(context: Context, resource: Int, list: List<PlaceDto>?) : ArrayAdapter<PlaceDto>(context,resource, list) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val item = getItem(position)

        if(convertView == null){
            val view = LayoutInflater.from(context).inflate(R.layout.item_place_autocomplete,parent,false)
            val nameTextView = view?.place_name_text_view
            nameTextView?.text = item.name
            return view
        }

        return convertView
    }

}