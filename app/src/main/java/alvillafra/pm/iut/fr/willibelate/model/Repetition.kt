package alvillafra.pm.iut.fr.willibelate.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

const val NEW_REPETITION_ID = 0L

@Entity(tableName = "repetition")
data class Repetition(
        var active : Boolean,
        var days : Days?,
        var timeBeforeDeparture : Time = Time(0,30),
        @PrimaryKey(autoGenerate = true) val key : Long = NEW_REPETITION_ID) : Serializable
