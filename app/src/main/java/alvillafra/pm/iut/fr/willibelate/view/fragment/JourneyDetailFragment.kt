package alvillafra.pm.iut.fr.willibelate.view.fragment

import alvillafra.pm.iut.fr.willibelate.R
import alvillafra.pm.iut.fr.willibelate.databinding.FragmentJourneyDetailBinding
import alvillafra.pm.iut.fr.willibelate.model.Journey
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import alvillafra.pm.iut.fr.willibelate.viewmodel.JourneyViewModel
import android.app.TimePickerDialog
import android.content.DialogInterface
import androidx.navigation.fragment.navArgs

import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_journey_detail.*
import kotlinx.android.synthetic.main.item_journey.*

/**
 * Created by alicevillafranca on 06/02/2019.
 */
class JourneyDetailFragment : Fragment(){

    private lateinit var journeyVM : JourneyViewModel
    private lateinit var navController : NavController

    private val args : JourneyDetailFragmentArgs by navArgs()


    companion object{
        private const val JOURNEY_ID = "com.alvillafra.willibelate.view.journey"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val journeyId = savedInstanceState?.getLong(JOURNEY_ID)?: args.journeyId

        journeyVM = ViewModelProviders.of(this, object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T = JourneyViewModel(journeyId) as T

        }).get(JourneyViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        navController = Navigation.findNavController(activity!!,R.id.nav_host_fragment)
        val binding = FragmentJourneyDetailBinding.inflate(inflater)
        binding.journeyVM = journeyVM
        binding.lifecycleOwner = this
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureRepetitionDialog()
        configureTimeBeforeDepartureDialog()

    }


    private fun configureTimeBeforeDepartureDialog() {
        time_before_departure_linear_layout.setOnClickListener {
            val timePicker = TimePickerDialog(context,
                TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                    journeyVM.journey.value?.let{
                        it.repetition.timeBeforeDeparture.hour = hour
                        it.repetition.timeBeforeDeparture.minute = minute
                    }
                },
                journeyVM.journey.value?.repetition!!.timeBeforeDeparture.hour,
                journeyVM.journey.value?.repetition!!.timeBeforeDeparture.minute,
                false)
            timePicker.setTitle(alvillafra.pm.iut.fr.willibelate.R.string.time_before_departure_text)
            timePicker.show()
        }
    }


    private fun configureRepetitionDialog(){
        repetition_linear_layout.setOnClickListener {
            val dialog = RepetitionPatternFragment()
            val bundle = Bundle()
            bundle.putSerializable(RepetitionPatternFragment.JOURNEY,journeyVM.journey.value)
            dialog.arguments = bundle
            dialog.show(fragmentManager,"pick_repetition_pattern")
            dialog.onDismiss(object : DialogInterface {
                override fun dismiss() {
                    journeyVM.update()
                }

                override fun cancel() {
                    journeyVM.update()
                }

            })
        }
    }
}