package alvillafra.pm.iut.fr.willibelate.view.fragment

import alvillafra.pm.iut.fr.willibelate.R
import alvillafra.pm.iut.fr.willibelate.model.Journey
import alvillafra.pm.iut.fr.willibelate.view.adapter.JourneyItemListAdapter
import alvillafra.pm.iut.fr.willibelate.viewmodel.JourneyListViewModel
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_journey_master.*
import java.lang.RuntimeException

class JourneyMasterFragment : Fragment(), JourneyItemListAdapter.Callback{



    private lateinit var listener : NavigationListener

    interface NavigationListener {
        fun onNavigateToDetail(id : Long)
        fun onNavigateToSearch()
        fun onNavigateToMaster()

    }


    override fun onItemCLicked(journey : Journey) {
        listener.onNavigateToDetail(journey.id)
    }

    override fun onSwitchActivated(journey: Journey, isActive: Boolean) {
        journey.repetition?.active = isActive
        journeyListVM.update(journey)
    }


    private lateinit var journeyListVM : JourneyListViewModel

    private val journeyAdapter = JourneyItemListAdapter(this)


    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if(context is NavigationListener){
            listener = context
        }
        else{
            RuntimeException("The context doesn't implement NavigationListener !")
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Initialize the ViewModel
        journeyListVM = ViewModelProviders.of(this).get(JourneyListViewModel::class.java)

        //Update the Recycler View Adapter when the VM changes
        journeyListVM.journeyList.observe(this, Observer { journeyAdapter.submitList(it) })

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_journey_master,container,false)

        val fab = view.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener{view ->
            listener.onNavigateToSearch()
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        master_recycler_view.adapter = journeyAdapter
    }
}