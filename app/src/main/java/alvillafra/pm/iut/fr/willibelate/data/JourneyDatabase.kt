package alvillafra.pm.iut.fr.willibelate.data

import alvillafra.pm.iut.fr.willibelate.JourneyApplication
import alvillafra.pm.iut.fr.willibelate.data.converter.Converters
import alvillafra.pm.iut.fr.willibelate.data.dao.JourneyDao
import alvillafra.pm.iut.fr.willibelate.model.*
import android.app.Application

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

import java.lang.RuntimeException

@Database(entities = [Journey::class, Days::class, Repetition::class, Station::class, Time::class], version = 1)
@TypeConverters(Converters::class)
abstract class JourneyDatabase : RoomDatabase(){

    abstract fun journeyDao() : JourneyDao

    companion object{
        private lateinit var application : Application
        @Volatile private var instance: JourneyDatabase?=null

        fun getInstance(): JourneyDatabase{
            if(application != null){
                if(instance == null){
                    synchronized(this) {
                        if (instance == null) {
                            instance = Room.databaseBuilder(
                                    application.applicationContext,
                                    JourneyDatabase::class.java,"journey-db").build()
                        }
                    }
                }
                return instance!!
            }

            else {
                throw RuntimeException()
            }
        }

        @Synchronized fun initialize(app : JourneyApplication){
                application = app
        }
    }
}