package alvillafra.pm.iut.fr.willibelate.view

import alvillafra.pm.iut.fr.willibelate.R
import alvillafra.pm.iut.fr.willibelate.model.Journey
import alvillafra.pm.iut.fr.willibelate.view.adapter.JourneyItemListAdapter
import alvillafra.pm.iut.fr.willibelate.view.fragment.JourneyMasterFragment
import alvillafra.pm.iut.fr.willibelate.view.fragment.JourneyMasterFragmentDirections
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), JourneyMasterFragment.NavigationListener{

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        navController =  Navigation.findNavController(this,R.id.nav_host_fragment)
        toolbar.setupWithNavController(navController,AppBarConfiguration(navController.graph))
    }

    override fun onNavigateToDetail(id: Long) {
        navController.navigate(JourneyMasterFragmentDirections.actionJourneyDetail(id))
    }

    override fun onNavigateToMaster() {
        // TODO : Find better way to go back to master
        while(navController.navigateUp()){}
    }

    override fun onNavigateToSearch() {
        navController.navigate(JourneyMasterFragmentDirections.actionJourneySearch())
    }

}